#include "Advanced2D.h"

namespace Advanced2D {

	Vector2::Vector2()
	{
		x = y = 0;
	}
	
	Vector2::Vector2( double x, double y ) 
	{ 
		Set( x, y ); 
	}

	void Vector2::Set(double x1,double y1) 
	{ 
		x=x1; y=y1;
	}
	
	//Vector2 length is distance from the origin
	double Vector2::Length()
	{
		return sqrt(x*x + y*y);
	}
	
	//dot/scalar product: difference between two directions
	double Vector2::DotProduct( const Vector2& v )
	{
		return (x*v.x + y*v.y);
	}

	Vector2 Vector2::Normal()
	{
		double length;
		if (Length() == 0)
			length = 0;
		else
			length = 1 / Length();
		double nx = x*length;
		double ny = y*length;
		return Vector2(nx,ny);
	}

	double Vector2::getX()
	{
		return x;
	}

	double Vector2::getY()
	{
		return y;
	}
}