#include "Advanced2D.h"

#define PIOVER180 0.01745329252

namespace Advanced2D
{
	Quaternion::Quaternion(float x, float y, float z, float w)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}

	// normalising a quaternion works similar to a vector. This method will not do anything
	// if the quaternion is close enough to being unit-length. define TOLERANCE as something
	// small like 0.00001f to get accurate results
	void Quaternion::normalize()
	{
		float mag2 = w*w + x*x + y*y + z*z;
		if(fabs(mag2) > 0.0000001f && fabs(mag2 - 1.0f) > 0.0000001f)
		{
			float mag = sqrt(mag2);
			x /= mag;
			y /= mag;
			z /= mag;
			w /= mag;
		}
	}

	Quaternion Quaternion::conjugate()
	{
		return Quaternion( -x, -y, -z, -w);
	}

	Quaternion Quaternion::operator*(Quaternion &q)
	{
		float nx = w * q.getX() + x * q.getW() + y * q.getZ() - z * q.getY();
		float ny = w * q.getY() + y * q.getW() + z * q.getX() - x * q.getZ();
		float nz = w * q.getZ() + z * q.getW() + x * q.getY() - y * q.getX();
		float nw = w * q.getW() - x * q.getX() - y * q.getY() - z * q.getZ();

		return Quaternion(nx, ny, nz, nw);
	}

	Vector3 Quaternion::times(Vector3 &v, int direction)
	{
		Vector3 vn(v);
		vn.Normal();

		Quaternion vecQuat, resQuat;
		vecQuat.setX(vn.getX());
		vecQuat.setY(vn.getY());
		vecQuat.setZ(vn.getZ());
		vecQuat.setW(0.0f);

		resQuat = vecQuat * conjugate();
		if(direction == LEFT)
			resQuat = *this * resQuat;
		else
			resQuat = resQuat * *this;

		return Vector3(resQuat.getX(), resQuat.getY(), resQuat.getZ());
	}

	void Quaternion::fromAxis(Vector3 &v, float angle)
	{
		float sinAngle;
		angle *= 0.5f;
		Vector3 vn(v);
		vn.Normal();
 
		sinAngle = sin(angle);
 
		x = (vn.getX() * sinAngle);
		y = (vn.getY() * sinAngle);
		z = (vn.getZ() * sinAngle);
		w = cos(angle);
	}

	void Quaternion::fromEuler(float pitch, float yaw, float roll)
	{
		
		// Basically we create 3 Quaternions, one for pitch, one for yaw, one for roll
		// and multiply those together.
		// the calculation below does the same, just shorter
 
		float p = pitch * PIOVER180 / 2.0;
		float y = yaw * PIOVER180 / 2.0;
		float r = roll * PIOVER180 / 2.0;
 
		float sinp = sin(p);
		float siny = sin(y);
		float sinr = sin(r);
		float cosp = cos(p);	
		float cosy = cos(y);
		float cosr = cos(r);
 
		this->x = sinr * cosp * cosy - cosr * sinp * siny;
		this->y = cosr * sinp * cosy + sinr * cosp * siny;
		this->z = cosr * cosp * siny - sinr * sinp * cosy;
		this->w = cosr * cosp * cosy + sinr * sinp * siny;
 
		normalize();
	}

	Matrix4 Quaternion::getMatrix()
	{
		float x2 = x * x;
		float y2 = y * y;
		float z2 = z * z;
		float xy = x * y;
		float xz = x * z;
		float yz = y * z;
		float wx = w * x;
		float wy = w * y;
		float wz = w * z;
 
		// This calculation would be a lot more complicated for non-unit length quaternions
		// Note: The constructor of Matrix4 expects the Matrix in column-major format like expected by
		//   OpenGL
		return Matrix4( 1.0f - 2.0f * (y2 + z2), 2.0f * (xy - wz), 2.0f * (xz + wy), 0.0f, 2.0f * (xy + wz), 1.0f - 2.0f * (x2 + z2), 2.0f * (yz - wx), 0.0f, 2.0f * (xz - wy), 2.0f * (yz + wx), 1.0f - 2.0f * (x2 + y2), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
	}

	void Quaternion::getAxisAngle(Vector3 *axis, float *angle)
	{
		float scale = sqrt(x * x + y * y + z * z);
		axis->setX(x / scale);
		axis->setY(y / scale);
		axis->setZ(z / scale);
		*angle = acos(w) * 2.0f;
	}
}