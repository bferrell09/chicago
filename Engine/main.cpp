#include "Advanced2D.h"

using namespace std;
using namespace Advanced2D;

int main(int argc, char *argv)
{
	Quaternion q(0,0,0,1);
	Quaternion temp(0,1,0,0);

	Quaternion result = q*temp;

	Vector3 vector = result.getPosition();

	cout << vector.getX() << " " << vector.getY() << " " << vector.getZ() << endl;

	return 0;
}