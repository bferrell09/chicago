#include "Advanced2D.h"

#pragma once

namespace Advanced2D {

	class Vector2 {
	private:
		double x, y;
	public:
		Vector2();
		Vector2(double x, double y);
		void Set(double x1,double y1);
		double Length();
		double DotProduct( const Vector2& v );
		Vector2 Normal();
		double getX();
		double getY();
    }; //class
};