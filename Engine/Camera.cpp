#include "Advanced2D.h"

#define PIOVER180 0.01745329252
//***mod--floats to doubles

//using namespace Advanced2D;

namespace Advanced2D
{
	Camera::Camera(void)
	{
		p_position = D3DXVECTOR3(0.0f,0.0f,10.0f);
		p_updir = D3DXVECTOR3(0.0f,1.0f,0.0f);
	
		xrot = 0.0f;
		yrot = 0.0f;
		xmov = 0.0f;
		ymov = 0.0f;
		zmov = 0.0f;
		rotspeed = 3.0f;
		movespeed = 3.0f;
		
		//hard coded to 1.3333 by default
		double ratio = 640 / 480;
		setPerspective(3.14159f / 4, ratio, 1.0f, 2000.0f);
	
	}

	Camera::~Camera(void)
	{
	}
	
	void Camera::setPerspective(double fov, double aspectRatio, double nearRange, double farRange)
	{
		this->setFOV(fov);
		this->setAspectRatio(aspectRatio);
		this->setNearRange(nearRange);
		this->setFarRange(farRange);
	}


	void Camera::Update()
	{
	 //set the camera's perspective matrix
	 D3DXMatrixPerspectiveFovLH(&this->p_matrixProj, (float)this->p_fov, 
			(float)this->p_aspectRatio, (float)this->p_nearRange, (float)this->p_farRange);
	 g_engine->getDevice()->SetTransform(D3DTS_PROJECTION, &this->p_matrixProj);
	
	 //set the camera's view matrix
	 D3DXMatrixLookAtLH(&this->p_matrixView, &this->p_position, &this->p_target, &this->p_updir);
	 g_engine->getDevice()->SetTransform(D3DTS_VIEW, &this->p_matrixView);
	}
	
	void Camera::setPosition(double x, double y, double z)
	{
		this->p_position.x = (float)x;
		this->p_position.y = (float)y;
		this->p_position.z = (float)z;
	}
	
	void Camera::setPosition(D3DXVECTOR3 position)
	{
		this->setPosition(position.x, position.y, position.z);
	}
	
	void Camera::setRotation(float xRot, float yRot, float zRot)
	{
		rotation.setX(xRot);
		rotation.setY(yRot);
		rotation.setZ(zRot);
	}

	void Camera::movex(float xmod)
	{
		rotation *= Vector3(xmod, 0.0f, 0.0f);
		p_position.x += rotation.getX();
		p_position.y += rotation.getY();
		p_position.z += rotation.getZ();
	}

	void Camera::movey(float ymod)
	{
		p_position.y -= ymod;
	}

	void Camera::movez(float zmod)
	{
		rotation *= Vector3(0.0f, 0.0f, -zmod);
		p_position.x += rotation.getX();
		p_position.y += rotation.getY();
		p_position.z += rotation.getZ();
	}

	void Camera::rotatex(float xrmod)
	{
		Quaternion nrot(1.0f, 0.0f, 0.0f, xrmod * PIOVER180);
		rotation = nrot.times(rotation, 1);
	}

	void Camera::rotatey(float yrmod)
	{
		Quaternion nrot(0.0f, 1.0f, 0.0f, yrmod * PIOVER180);
		rotation = nrot.times(rotation, 0);
	}

	void Camera::tick(float seconds)
	{
		if(xrot != 0.0f)
			rotatex(xrot * seconds * rotspeed);
		if(yrot != 0.0f)
			rotatey(yrot * seconds * rotspeed);
		if(xmov != 0.0f)
			movex(xmov * seconds * movespeed);
		if(ymov != 0.0f)
			movey(ymov * seconds * movespeed);
		if(zmov != 0.0f)
			movez(zmov * seconds * movespeed);
	}
};