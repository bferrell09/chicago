#include "Advanced2D.h"

#pragma once

namespace Advanced2D
{
	class Matrix4
	{
	public:
		Matrix4(float x11, float x21, float x31, float x41, float x12, float x22, float x32, float x42, float x13, float x23, float x33, float x43, float x14, float x24, float x34, float x44);
		float x11;
		float x12;
		float x13;
		float x14;
		float x21;
		float x22;
		float x23;
		float x24;
		float x31;
		float x32;
		float x33;
		float x34;
		float x41;
		float x42;
		float x43;
		float x44;
	};
};