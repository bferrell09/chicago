#include "Advanced2D.h"

namespace Advanced2D {

	Vector3::Vector3()
	{
		x = y = z = 0;
	}
	
	Vector3::Vector3( const Vector3& v ) 
	{ 
		*this = v; 
	}
	
	Vector3::Vector3( double x, double y, double z ) 
	{ 
		Set( x, y, z ); 
	}
	
	Vector3::Vector3( int x, int y, int z)
	{ 
		Set((double)x,(double)y,(double)z); 
	}
	
	void Vector3::Set( double x1,double y1,double z1 ) 
	{ 
		x=x1; y=y1; z=z1; 
	}
	
	void Vector3::Set( const Vector3& v) 
	{ 
		x=v.x; y=v.y; z=v.z; 
	}
	
	void Vector3::Move( double mx,double my,double mz) 
	{ 
		x+=mx; y+=my; z+=mz; 
	}

	void Vector3::operator+=(const Vector3& v) 
	{ 
		x+=v.x; y+=v.y; z+=v.z; 
	}
	
	void Vector3::operator-=(const Vector3& v) 
	{ 
		x-=v.x; y-=v.y; z-=v.z; 
	}
	
	void Vector3::operator*=(const Vector3& v) 
	{ 
		x*=v.x; y*=v.y; z*=v.z; 
	}
	
	void Vector3::operator/=(const Vector3& v) 
	{ 
		x/=v.x; y/=v.y; z/=v.z; 
	}
	
	//equality operator comparison includes double rounding
	bool Vector3::operator==( const Vector3& v ) const
	{ 
		return (
			(((v.x - 0.0001f) < x) && (x < (v.x + 0.0001f))) &&
			(((v.y - 0.0001f) < y) && (y < (v.y + 0.0001f))) &&
			(((v.z - 0.0001f) < z) && (z < (v.z + 0.0001f))) );
	}
	
	//inequality operator
	bool Vector3::operator!=( const Vector3& p ) const 
	{
		return (!(*this == p));
	}
	
	//assign operator
	Vector3& Vector3::operator=( const Vector3& v) 
	{
		Set(v);
		return *this;
	}
	
	//distance only coded for 2D
	double Vector3::Distance( const Vector3& v ) 
	{
		return sqrt((v.x-x)*(v.x-x) + (v.y-y)*(v.y-y));
	}
	
	//Vector3 length is distance from the origin
	double Vector3::Length()
	{
		return sqrt(x*x + y*y + z*z);
	}
	
	//dot/scalar product: difference between two directions
	double Vector3::DotProduct( const Vector3& v )
	{
		return (x*v.x + y*v.y + z*v.z);
	}
	
	//cross/Vector product is used to calculate the normal
	Vector3 Vector3::CrossProduct( const Vector3& v )
	{
		double nx = (y*v.z)-(z*v.y);
		double ny = (z*v.y)-(x*v.z);
		double nz = (x*v.y)-(y*v.x);
		return Vector3(nx,ny,nz);
	}
	
	Vector3 Vector3::CrossProduct(const Vector3& v1, const Vector3& v2)
	{
		return Vector3((v1.y*v2.z)-(v1.z*v2.y), 
						(v1.z*v2.y)-(v1.x*v2.z),
						(v1.x*v2.y)-(v1.y*v2.x));
	}

	Vector3 Vector3::operator-(const Vector3& v)
	{
		Vector3 temp(*this);
		temp -= v;
		return temp;
	}

	//calculate normal angle of the Vector
	Vector3 Vector3::Normal()
	{
		double length;
		if (Length() == 0)
			length = 0;
		else
			length = 1 / Length();
		double nx = x*length;
		double ny = y*length;
		double nz = z*length;
		return Vector3(nx,ny,nz);
	}
	
	Vector3 Vector3::XAxis(double theta)
	{
		double xP = x;
		double yP = y*cos(theta) - z*sin(theta);
		double zP = y*sin(theta) + z*cos(theta);

		Vector3 result(xP, yP, zP);

		return result;
	}

	Vector3 Vector3::YAxis(double theta)
	{
		double xp = x*cos(theta) + z*sin(theta);
		double yp = y;
		double zp = -x*sin(theta) + z*cos(theta);

		Vector3 result(xp, yp, zp);

		return result;
	}
    
    void Vector3::rotate(const Vector3& rAxis, double degree) {
		double u = rAxis.x;
        double v = rAxis.y;
        double w = rAxis.z;
        double l = u*u + v*v + w*w;
        double cosT = cos(degree * 3.14159 / 180);
        double sinT = sin(degree * 3.14159 / 180);
        double uxvywz = (u*x + v*y + w*z)*(1-cosT);
        double sqrtL = sqrt(l);
        double xr = u*uxvywz+l*x*cosT+sqrtL*(v*z - w*y)*sinT;
        xr /= l;
        double yr = v*uxvywz+l*y*cosT+sqrtL*(w*x - u*z)*sinT;
        yr /= l;
        double zr = w*uxvywz+l*z*cosT+sqrtL*(u*y - v*x)*sinT;
        zr /= l;
        
		x=xr;
		y=yr;
		z=zr;
    }

	Vector3 Vector3::scalar(double k)
	{
		return Vector3(k*x, k*y, k*z);
	}

	Vector3 Vector3::getFinalPosition(Vector3 position, Vector3 velocity, Vector3 acceleration, double time)
	{
		Vector3 newPosition = position;
		newPosition += velocity.scalar(time);
		newPosition += acceleration.scalar(pow(time, 2)/2);
		return newPosition;
	}

	Vector3 Vector3::getFinalVelocity(Vector3 velocity, Vector3 acceleration, double time)
	{
		Vector3 newVelocity = velocity;
		newVelocity += acceleration.scalar(time);
		return newVelocity;
	}
}
