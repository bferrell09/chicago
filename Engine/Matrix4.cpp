#include "Advanced2D.h"

namespace Advanced2D
{
	Matrix4::Matrix4(float x11, float x21, float x31, float x41, float x12, float x22, float x32, float x42, float x13, float x23, float x33, float x43, float x14, float x24, float x34, float x44)
	{
		this->x11 = x11;
		this->x12 = x12;
		this->x13 = x13;
		this->x14 = x14;
		this->x21 = x21;
		this->x22 = x22;
		this->x23 = x23;
		this->x24 = x24;
		this->x31 = x31;
		this->x32 = x32;
		this->x33 = x33;
		this->x34 = x34;
		this->x41 = x41;
		this->x42 = x42;
		this->x43 = x43;
		this->x44 = x44;
	}
}