#include "Advanced2D.h"

#pragma once
#define LEFT 0
#define RIGHT 1

namespace Advanced2D
{
	class Matrix4;
	class Quaternion
	{

	private:
		float x;
		float y;
		float z;
		float w;
	public:
		Quaternion(float x = 0.0f, float y = 0.0f, float z = 0.0f, float w = 0.0f);
		void normalize();
		Quaternion conjugate();
		Quaternion operator*(Quaternion &q);
		Vector3 times(Vector3 &v, int direction);
		float getX() {return x; }
		void setX(float x) {this->x = x; }
		float getY() {return y; }
		void setY(float y) {this->y = y; }
		float getZ() {return z; }
		void setZ(float Z) {this->z = z; }
		float getW() {return w; }
		void setW(float w) {this->w = w; }
		void fromAxis(Vector3 &v, float angle);
		void fromEuler(float pitch, float yaw, float roll);
		Matrix4 getMatrix();
		void getAxisAngle(Vector3 *axis, float *angle);
	};
};