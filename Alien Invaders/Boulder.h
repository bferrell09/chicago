#include "..\Engine\Advanced2D.h"

using namespace Advanced2D;

class Boulder
{
private:
	bool isAlive;
	int magnitude; //class size of boulder: 1 being smallest, 3 being biggest
	double radius;
	Vector3 orbitalAxis;
	double speed;
	D3DXMATRIX internRotation;
	D3DXMATRIX rotation;

	Vector3 velocity;
	Vector3 acceleration;
	double mass;
	double time;
	Vector3 left;
	Vector3 forward;
	Vector3 up;
public:
	Mesh *item;
	Boulder(int mag, Vector3 position, Vector3 velocity, double time, double speed);
	int getMagnitude() { return magnitude; }
	void setAxis(Vector3 a) {orbitalAxis = a;}
	void setAxis(double x, double y, double z) {orbitalAxis.setX(x); orbitalAxis.setY(y); orbitalAxis.setZ(z);}
	Vector3 getAxis() {return orbitalAxis;}
	void Load(char *image) {item->Load(image);}
	void setPosition(double x, double y, double z);
	void setPosition(Vector3 p);
	double getRadius() {return radius;}
	Vector3 getVelocity() { return velocity; }
	double getSpeed() { return speed; }
	void setSpeed(double s) { speed = s; }
	double getMass() { return mass; }
	bool getAlive() { return isAlive; }
	void setAlive(bool a) { isAlive = a; }
	Vector3 getPosition();
	void setOrientation(Vector3 l, Vector3 f, Vector3 u);
	void setOrientation(double xl, double yl, double zl, double xf, double yf, double zf, double xu, double yu, double zu);
	void update();
	void draw();
};