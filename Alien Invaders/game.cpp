/**
* ALIEN INVADERS
*/


#include "..\Engine\Advanced2D.h"
#include "Boulder.h"
#include "PowerUp.h"
#include "Enemy.h"
#include <math.h>
#include <cmath>
#include <vector>
#include <time.h>
using namespace Advanced2D;
using namespace std;

#define BLACK D3DCOLOR_XRGB(0,0,0)
#define RED D3DCOLOR_XRGB(255,0,0)
#define PI 3.14159265

struct weapon {
	Vector3 forward;
	Vector3 up;
	Vector3 velocityAxis; // aka left
	double velocity;
	int ttl;
	Mesh *style;
	bool alive;
};

Vector3 *cF;
Vector3 *cT;
Vector3 *shipForward;
Vector3 *shipLeft;
Vector3 *shipUp;
Vector3 *screenForward;
Vector3 *screenLeft;
Mesh *earth;
Mesh *ship;
Mesh *bullet;

Timer lastShotFired;
Timer spawnNewBoulder;
Timer boostRecharge;
double boostDuration = 500;
double boostWait = 5000 + boostDuration;

// shieldVars
bool shieldActive = false;
Timer shieldTimer;
int shieldPercentage = 50;
int shieldDecrease = 15;
int shieldIncrease = 750;
int maxShield = 50;


Camera *camera;
Console *console;
Texture *fontImg;
Font *font24;
Font *font10;

D3DXMATRIX shipRotMat;
vector<weapon> shotsFired;
vector<Boulder> boulders;
vector<PowerUp> powerUps;
vector<Enemy> enemies;

int xMouse = 0;
int yMouse = 0;
double xCam = 0.0;
double yCam = 0.0;
double zCam = 0.0;
double speedFactor = 1.50;
double shipRotate = 0;
int score = 0;
int lives = 5;
int state = 0;
double angle = 90.0;
int lightDir = -1;
double shipvelX = 0.0;
double shipvelY = 0.0;
double playerx, playery;
double distFromPlanet = 63.0;
bool createNewBoulder = true;
bool boostActive;
bool gunUp = false;

int mButton = 0;

int testing = 0;

bool game_preload()
{
	g_engine->setAppTitle("CHICAGO");
	g_engine->setFullscreen(false);
	g_engine->setScreenWidth(1024);
	g_engine->setScreenHeight(768);
	g_engine->setColorDepth(32);
	return 1;
}


void resetGame()
{
	//reset variables
	lives = 5;
	score = 0;
}


bool game_init(HWND hwnd)
{
	//create the console
	console = new Console();
	if (!console->init()) {
		g_engine->message("Error creating console");
		return false;
	}
	console->hide();

	//load the Verdana10 font
	font10 = new Font();
	if (!font10->loadImage("verdana10.tga")) {
		g_engine->message("Error loading verdana10.tga");
		return false;
	}
	if (!font10->loadWidthData("verdana10.dat")) {
		g_engine->message("Error loading verdana10.dat");
		return false;
	}
	font10->setColumns(16);
	font10->setCharSize(20,16);

	//load the TimesNewRoman24 font
	font24 = new Font();
	if (!font24->loadImage("timesnewroman24.tga")) {
		g_engine->message("Error loading timesnewroman24.tga");
		return false;
	}
	if (!font24->loadWidthData("timesnewroman24.dat")) {
		g_engine->message("Error loading timesnewroman24.dat");
		return false;
	}
	font24->setColumns(16);
	font24->setCharSize(32,36);

	//load audio files
	g_engine->audio->Load("../music/halo-3-soundtrack-unforgotten-hq.mp3", "unforgotten");
	g_engine->audio->Load("fire1.wav", "gunfire");

	ship = new Mesh();
	ship->Load("ship.x");
	ship->SetPosition(0.0, 0.0, distFromPlanet);
	ship->SetScale(0.03, 0.03, 0.03);
	ship->SetRotation(0.0, 90.0, 0.0);

	screenForward = new Vector3(1.0, 0.0, 0.0);
	shipForward = new Vector3(0.0, 0.0, 0.0);
	shipUp = new Vector3(0.0, 0.0, 0.0);
	screenLeft = new Vector3(0.0, 1.0, 0.0);
	shipLeft = new Vector3(0.0, 0.0, 0.0);

	//set up lighting
	g_engine->SetAmbient(D3DCOLOR_RGBA(80,80,80, 0));

	//load Earth for background 
	earth = new Mesh();
	earth->Load("earth.x");
	earth->SetPosition(0.0f, 0.0f, 0.0f);
	earth->SetScale(2.5, 2.5, 2.5);

	//set the camera and perspective
	camera = new Camera();
	camera->setPosition(0.0f, 0.0f, 150.0f);
	camera->setTarget(ship->GetPosition());
	camera->Update();

	cF = new Vector3(1.0, 0.0, 150.0);
	cT = new Vector3(0.0, 1.0, 150.0);

	// load weapons
	bullet = new Mesh();
	bullet->Load("bullet.x");
	bullet->SetScale(2.0, 2.0, 2.0);

	//reset the game
	resetGame();

	g_engine->audio->Play("unforgotten");

	//begin in pause mode
	g_engine->setPaused(true);

	return true;
}

void game_update()
{
	//game state
	switch(state)
	{
	case 0: //title
		g_engine->setPaused(true);
		break;

	case 1: //normal
		break;

	case 2: //gameover
		g_engine->setPaused(true);
		break;
	}
}

void updateWeapons() {
	for(int i = 0; i < shotsFired.size(); i++) {
		Vector3 pos(shotsFired[i].style->GetPosition().x,
					shotsFired[i].style->GetPosition().y,
					shotsFired[i].style->GetPosition().z);
		pos.rotate(shotsFired[i].velocityAxis,shotsFired[i].velocity);
		shotsFired[i].style->SetPosition(pos.getX(), pos.getY(), pos.getZ());

		shotsFired[i].forward.rotate(shotsFired[i].velocityAxis,shotsFired[i].velocity);
		
		D3DXMATRIX rot;
							
		rot._11 = -shotsFired[i].forward.getX();
		rot._12 = -shotsFired[i].forward.getY();
		rot._13 = -shotsFired[i].forward.getZ();
		rot._14 = 0.0;
		rot._21 = shotsFired[i].velocityAxis.getX();
		rot._22 = shotsFired[i].velocityAxis.getY();
		rot._23 = shotsFired[i].velocityAxis.getZ();
		rot._24 = 0.0;
		rot._31 = shotsFired[i].up.getX();
		rot._32 = shotsFired[i].up.getY();
		rot._33 = shotsFired[i].up.getZ();
		rot._34 = 0.0;
		rot._41 = 0.0;
		rot._42 = 0.0;
		rot._43 = 0.0;
		rot._44 = 1.0;

		shotsFired[i].style->draw(rot);
		shotsFired[i].ttl--;
	}
	for(int i = 0; i < shotsFired.size(); i++) {
		if(shotsFired[i].ttl == 0 || !shotsFired[i].alive) {
			delete shotsFired[i].style;
			shotsFired.erase(shotsFired.begin()+i);
		}
	}
}

bool boulderAtLocation(Vector3 a) {
	for (int i = 0; i < boulders.size(); i++)
	{
		Vector3 b(boulders[i].getPosition());
		double dist = pow((a.getX() - b.getX()),2) +
					  pow((a.getY() - b.getY()),2) +
					  pow((a.getZ() - b.getZ()),2);
		double radius = pow((10 + boulders[i].getRadius()),2);
		if(dist <= radius)
			return false;
	}
	return true;
}

void updateBoulders() 
{
	if(spawnNewBoulder.stopwatch(2500) && boulders.size() < 15)
	{
		srand(time(NULL));
		double degreeX = rand() % 320 + 20; 
		double degreeY = rand() % 320 + 20;
		Vector3 pos(ship->GetPosition().x,ship->GetPosition().y,ship->GetPosition().z);
		pos.rotate(*screenForward,degreeX);
		pos.rotate(*screenLeft, degreeY);
		while(!boulderAtLocation(pos))
		{
			pos.rotate(*screenForward,22.5);
			pos.rotate(*screenLeft, 22.5);
		}
		Boulder b = Boulder(3,pos, Vector3(0.01, 0.01, 0.01), 1000/(double)g_engine->getFrameRate_core(), 0.3);
		boulders.push_back(b);
		//createNewBoulder = false;
	}
	for(int i = 0; i < boulders.size(); i++)
	{
		Vector3 b(boulders[i].getPosition());
		double radius = pow((boulders[i].getRadius() + 1.0), 2);
		for(int j = 0; j < shotsFired.size(); j++)
		{
			// Collision detection between bullet and boulder
			Vector3 a(shotsFired[j].style->GetPosition().x,
					 shotsFired[j].style->GetPosition().y,
					 shotsFired[j].style->GetPosition().z);
			double dist = pow((a.getX() - b.getX()),2) +
						  pow((a.getY() - b.getY()),2) +
						  pow((a.getZ() - b.getZ()),2);

			if(dist <= radius && shotsFired[j].alive)
			{
				shotsFired[j].alive = false;
				boulders[i].setAlive(false);
				// spawn children
				int mag = boulders[i].getMagnitude() - 1;
				if(mag > 0) 
				{
					// get new position for spawned boulders
					Vector3 pos1 = boulders[i].getPosition();
					Vector3 pos2 = boulders[i].getPosition();
					double theta = 0.6 * boulders[i].getRadius();
					pos1.rotate(*shipForward, theta);
					pos2.rotate(*shipForward, -theta);
					Vector3 vel = boulders[i].getVelocity();
					double speed = 1.5 * boulders[i].getSpeed();

					// create new boulders
					Boulder b1 = Boulder(mag, pos1, vel.scalar(2), 0, speed);
					Boulder b2 = Boulder(mag, pos2, vel.scalar(-2), 0, speed);

					boulders.push_back(b1);
					boulders.push_back(b2);
				}

				srand(time(NULL));
				int random = rand() % 1 + 1;
				if(random == 1)
				{
					srand(time(NULL));
					random = rand() % 100 + 1;
					int id;
					if(random % 2 == 0)
						id = 1;
					else if(random % 3 == 0)
						id = 2;
					else
						id = 3;

					PowerUp p(id, boulders[i].getPosition(), boulders[i].getVelocity());
					powerUps.push_back(p);
				}

			}
		}
		if(boulders[i].getAlive())
		{
			Vector3 axisNew(boulders[i].getPosition());
			axisNew = Vector3(axisNew.getX() - ship->GetPosition().x, 
							  axisNew.getY() - ship->GetPosition().y, 
						      axisNew.getZ() - ship->GetPosition().z);
			axisNew = axisNew.CrossProduct(*shipUp);
			//boulders[i].setAxis(axisNew);
			boulders[i].update();
			boulders[i].draw();
		}
	}
	// clean up!
	for (int i = boulders.size() - 1; i >= 0; i--)
	{
		if(!boulders[i].getAlive())
		{
			delete boulders[i].item;
			boulders.erase(boulders.begin()+i);
		}
	}
	// boulder-boulder collision detection
	for (int i = 0; i < boulders.size(); i++)
	{
		Vector3 a(boulders[i].getPosition());
		for (int j = i + 1; j < boulders.size(); j++)
		{
			Vector3 b(boulders[j].getPosition());
			double dist = pow((a.getX() - b.getX()),2) +
						  pow((a.getY() - b.getY()),2) +
						  pow((a.getZ() - b.getZ()),2);
			double radius = pow((boulders[i].getRadius() + boulders[j].getRadius()),2);

			if(dist <= radius)
			{
				Vector3 x1(boulders[i].getAxis());
				Vector3 x2(boulders[j].getAxis());
				Vector3 n1(boulders[i].getPosition().scalar(-1));
				Vector3 n2(boulders[j].getPosition().scalar(-1));
				double m1 = boulders[i].getMass();
				double m2 = boulders[j].getMass();
				double s1 = boulders[i].getSpeed();
				double s2 = boulders[j].getSpeed();

				Vector3 xf1(x2);//.CrossProduct(n2).scalar(m2/m1).CrossProduct(n1));
				xf1.CrossProduct(n2);
				xf1.scalar(m2/m1);
				xf1.CrossProduct(n1);

				Vector3 xf2(x1);
				xf2.CrossProduct(n1);
				xf2.scalar(m1/m2);
				xf2.CrossProduct(n2);

				double sf1 = m2 * s2 / m1;
				double sf2 = m1 * s1 / m2;

				double angle = acos(x1.DotProduct(x2) / (x1.Length() * x2.Length()));
				if (angle < .75)
					sf2 = -sf2;

				boulders[i].setAxis(xf1);
				boulders[i].setSpeed(sf1);
				boulders[j].setAxis(xf2);
				boulders[j].setSpeed(sf2);

				/*Vector3 tempAxis(boulders[i].getAxis());
				boulders[i].setAxis(boulders[j].getAxis());
				boulders[j].setAxis(tempAxis);
				/*if((dist / radius) <= .95)
				{
					Vector3 p(boulders[i].getPosition());
					Vector3 v(p);
					v -= boulders[j].getPosition();
					v = v.CrossProduct(p);
					p.rotate(v, 2.5);
				}*/
			}
		}
	}
}

void updatePowerUps()
{
	for(int i = 0; i < powerUps.size(); i++)
	{
		Vector3 b(powerUps[i].getPosition());
		double radius = pow((powerUps[i].getRadius() + 1.0), 2);
			// Collision detection between bullet and boulder
			Vector3 a(ship->GetPosition().x,
					 ship->GetPosition().y,
					 ship->GetPosition().z);
			double dist = pow((a.getX() - b.getX()),2) +
						  pow((a.getY() - b.getY()),2) +
						  pow((a.getZ() - b.getZ()),2);

		if(dist <= radius && powerUps[i].getAlive())
		{
			powerUps[i].setAlive(false);
		}
		if(powerUps[i].getAlive())
		{
			Vector3 axisNew(powerUps[i].getPosition());
			axisNew = Vector3(axisNew.getX() - ship->GetPosition().x, 
							  axisNew.getY() - ship->GetPosition().y, 
						      axisNew.getZ() - ship->GetPosition().z);
			axisNew = axisNew.CrossProduct(*shipUp);
			powerUps[i].setAxis(axisNew);
			powerUps[i].update();
			powerUps[i].draw();
		}
	}
	for (int i = powerUps.size() - 1; i >= 0; i--)
	{
		if(!powerUps[i].getAlive())
		{
			delete powerUps[i].item;
			powerUps.erase(powerUps.begin()+i);
		}
	}
}

void updateEnemy()
{
	srand(time(NULL));
	int random = rand() % 100 + 1;
	if(random % 5 == 0)
	{
		double degreeX = rand() % 320 + 20; 
		double degreeY = rand() % 320 + 20;
		Vector3 pos(ship->GetPosition().x,ship->GetPosition().y,ship->GetPosition().z);
		pos.rotate(*screenForward,degreeX);
		pos.rotate(*screenLeft, degreeY);
		Enemy e(enemies.size(), pos, Vector3(0.1, 0.1, 0.1));
		enemies.push_back(e);
	}

	for(int i = 0; i < enemies.size(); i++)
	{
		enemies[i].update();
		enemies[i].draw();
	}
	for(int i = 0; i < enemies.size(); i++)
	{
		Vector3 b(enemies[i].getPosition());
		double radius = pow((boulders[i].getRadius() + 1.0), 2);
		for(int j = 0; j < shotsFired.size(); j++)
		{
			// Collision detection between bullet and boulder
			Vector3 a(shotsFired[j].style->GetPosition().x,
					 shotsFired[j].style->GetPosition().y,
					 shotsFired[j].style->GetPosition().z);
			double dist = pow((a.getX() - b.getX()),2) +
						  pow((a.getY() - b.getY()),2) +
						  pow((a.getZ() - b.getZ()),2);

			if(dist <= radius && shotsFired[j].alive)
			{
				shotsFired[j].alive = false;
				enemies[i].setAlive(false);
			}
		}
	}
	for (int i = enemies.size() - 1; i >= 0; i--)
	{
		if(!enemies[i].getAlive())
		{
			delete enemies[i].item;
			enemies.erase(enemies.begin()+i);
		}
	}
}

//3D rendering with timing
void game_render3d()
{
	g_engine->ClearScene(BLACK);
	g_engine->SetIdentity();

	camera->Update();
	earth->draw();
	ship->draw(shipRotMat);

	updateWeapons();
	updateBoulders();
	updatePowerUps();
	updateEnemy();
}

//2D rendering with timing
void game_render2d()
{
	static ostringstream ostr;
	if(!shieldActive && shieldPercentage < maxShield && shieldTimer.stopwatch(shieldIncrease))
		shieldPercentage++;

	//game state
	switch(state)
	{
	case 0: //title
		font24->Print(320,300,"C H I C A G O", RED);
		font24->Print(380,600,"press space to start", RED);
		break;

	case 1: //normal
		ostr.str("");
		ostr << "Shield Percentage" << shieldPercentage;
		font24->Print(40,1,ostr.str(),0xDDEE4444);

		ostr.str("");
		ostr << "X position: " << xMouse;
		ostr << "\tY position: " << yMouse;
		font24->Print(300,1,ostr.str(),0xDDEE4444);

		ostr.str("");
		ostr << "Vx: " << xCam;
		ostr << "\tVy: " << yCam;
		ostr << "\tVz: " << zCam;
		font24->Print(300,50,ostr.str(),0xDDEE4444);

		ostr.str("");
		ostr << "LIVES " << lives;
		font24->Print(850,1,ostr.str(),0xDDEE4444);

		break;

	case 2: //gameover
		font24->Print(370,300,"G A M E    O V E R");
		font24->Print(400,600,"press any key");
		break;

	case 3: //paused
		font24->Print(350,300,"G A M E    P A U S E D");
		font24->Print(380,650,"press p to resume");
		break;
	}

	//update console
	if (console->isShowing()) {
		ostr.str("");
		ostr << "Core refresh: " << fixed << setprecision(4) 
			<< 1000.0 / (double)g_engine->getFrameRate_core() << " ms";
		console->print(ostr.str(),1);

		ostr.str("");
		ostr << "Screen refresh: "
			<< 1000.0 / (double)g_engine->getFrameRate_real() << " ms";
		console->print(ostr.str(),2);

		ostr.str("");
		ostr << "Player: " << playerx << "," << playery;
		console->print(ostr.str(),27);

		ostr.str("");
		ostr << "Forward: <" << shipForward->getX() << ", " << shipForward->getY() << ", " << shipForward->getZ() << ">";
		console->print(ostr.str(),3);

		ostr.str("");
		ostr << "Rotational Matrix";
		console->print(ostr.str(),4);

		ostr.str("");
		ostr << "[" << shipRotMat._11 << ", " << shipRotMat._12 << ", " << shipRotMat._13 << ", " << shipRotMat._14 << "]\n";
		console->print(ostr.str(),5);

		ostr.str("");
		ostr << "[" << shipRotMat._21 << ", " << shipRotMat._22 << ", " << shipRotMat._23 << ", " << shipRotMat._24 << "]\n";
		console->print(ostr.str(),6);

		ostr.str("");
		ostr << "[" << shipRotMat._31 << ", " << shipRotMat._32 << ", " << shipRotMat._33 << ", " << shipRotMat._34 << "]\n";
		console->print(ostr.str(),7);

		ostr.str("");
		ostr << "[" << shipRotMat._41 << ", " << shipRotMat._42 << ", " << shipRotMat._43 << ", " << shipRotMat._44 << "]\n";
		console->print(ostr.str(),8);

		ostr.str("");
		ostr << "Mouse Button: " << mButton;
		console->print(ostr.str(),9);

		ostr.str("");
		ostr << "Number of shots: " << shotsFired.size();
		console->print(ostr.str(),10);

		ostr.str("");
		ostr << "Boulder locatoin: "; 
		ostr << boulders[0].getPosition().getX() << ", ";
		ostr << boulders[0].getPosition().getY() << ", ";
		ostr << boulders[0].getPosition().getZ();
		console->print(ostr.str(),11);

		console->draw();


	}
}

void setShipRotMat() {
	shipRotMat._11 = -shipForward->getX();
	shipRotMat._21 = shipLeft->getX();//-shipForward->getY();
	shipRotMat._31 = shipUp->getX();//-shipForward->getZ();
	shipRotMat._41 = 0.0;
	shipRotMat._12 = -shipForward->getY();//shipLeft->getX();
	shipRotMat._22 = shipLeft->getY();
	shipRotMat._32 = shipUp->getY();//shipLeft->getZ();
	shipRotMat._42 = 0;
	shipRotMat._13 = -shipForward->getZ();//shipUp->getX();
	shipRotMat._23 = shipLeft->getZ();//shipUp->getY();
	shipRotMat._33 = shipUp->getZ();
	shipRotMat._43 = 0;
	shipRotMat._14 = 0;
	shipRotMat._24 = 0;
	shipRotMat._34 = 0;
	shipRotMat._44 = 1;
}

// Moving ship and camera around the planet
void update_ship() {
	if(boostActive && !boostRecharge.stopwatch(boostDuration))
	{
		speedFactor = 0.5;
	} else
	{
		speedFactor = 1.5;
		boostActive = false;
	}
	
	// angle of the rotation axis
	double dx = xMouse - g_engine->getScreenWidth() / 2;
    double dy = yMouse - g_engine->getScreenHeight() / 2;
    double h = sqrt(dx*dx+dy*dy);
    dx /= (h * speedFactor);
    dy /= (-h * speedFactor);
    
	// creating the rotation axis
    Vector3 rAy(camera->getX() - cF->getX(), camera->getY() - cF->getY(), camera->getZ() - cF->getZ());
    Vector3 rAx(camera->getX() - cT->getX(), camera->getY() - cT->getY(), camera->getZ() - cT->getZ());
	
	// camera vectors rotated
	cF->rotate(rAx, dx);
    cT->rotate(rAx, dx);
    cF->rotate(rAy, dy);
    cT->rotate(rAy, dy);

	// screen vectors rotated
	screenForward->rotate(rAx, dx);
	screenForward->rotate(rAy, dy);
	shipForward->rotate(rAx, dx);
	shipForward->rotate(rAy, dy);
	screenLeft->rotate(rAx, dx);
	screenLeft->rotate(rAy, dy);
	shipLeft->rotate(rAx, dx);
	shipLeft->rotate(rAy, dy);

	// rotating camera
    Vector3 cam(camera->getX(), camera->getY(), camera->getZ());
    cam.rotate(rAx, dx);
    cam.rotate(rAy, dy);
    camera->setPosition(cam.getX(), cam.getY(), cam.getZ());
    camera->setUp(cT->getX() - cam.getX(), cT->getY() - cam.getY(), cT->getZ() - cam.getZ());

	// Store current normal vector
	//Vector3 oldNorm(ship->GetPosition().x, ship->GetPosition().y, ship->GetPosition().z);

	// setting new ship position
    Vector3 shipPos(ship->GetPosition().x, ship->GetPosition().y, ship->GetPosition().z);
    shipPos.rotate(rAx, dx);
    shipPos.rotate(rAy, dy);
    ship->SetPosition(shipPos.getX(), shipPos.getY(), shipPos.getZ());
	//ship->Transform();

	camera->setTarget(ship->GetPosition());
	shipUp->Set(ship->GetPosition().x, ship->GetPosition().y, ship->GetPosition().z);
	shipUp->Set(shipUp->Normal());
	
	setShipRotMat();
	// rotate forward and left vectors to mouse on screen
    xCam = ship->GetPosition().x;
    yCam = ship->GetPosition().y;
    zCam = ship->GetPosition().z;
}

void fireShot() {
	if(!gunUp)
	{
		weapon newShot;
		newShot.style = new Mesh();
		newShot.style->Load("ball.x");
		//memcpy(&newShot.style,bullet,sizeof(Mesh));
		newShot.style->SetScale(1.0, 1.0, 1.0);
		newShot.velocity = -1.25;
		memcpy(&newShot.velocityAxis,shipForward,sizeof(Vector3));
		newShot.style->SetPosition(ship->GetPosition());
		newShot.style->SetRotation(ship->GetRotationX(), ship->GetRotationY(), ship->GetRotationZ());
		newShot.ttl = 75;
		memcpy(&newShot.forward,shipLeft,sizeof(Vector3));
		memcpy(&newShot.up,shipUp,sizeof(Vector3));
		newShot.alive = true;
		shotsFired.push_back(newShot);
	}
	else
	{
		double degree = -15.0;
		for(int i = 0; i < 3; i++)
		{
			weapon newShot;
			newShot.style = new Mesh();
			newShot.style->Load("ball.x");
			newShot.style->SetScale(1.0, 1.0, 1.0);
			newShot.velocity = -1.25;
			memcpy(&newShot.velocityAxis,shipForward,sizeof(Vector3));
			newShot.velocityAxis.rotate(Vector3(ship->GetPosition().x, ship->GetPosition().y, ship->GetPosition().z), degree);
			newShot.style->SetPosition(ship->GetPosition());
			newShot.style->SetRotation(ship->GetRotationX(), ship->GetRotationY(), ship->GetRotationZ());
			newShot.ttl = 20;
			memcpy(&newShot.forward,shipLeft,sizeof(Vector3));
			memcpy(&newShot.up,shipUp,sizeof(Vector3));
			newShot.alive = true;
			shotsFired.push_back(newShot);
			degree += 15;
		}
	}
}

void game_keyPress(int key)
{
	switch(key)
	{
	case DIK_LCONTROL:
		{
			update_ship();
			break;
		}
	case DIK_SPACE:
		{
			if(boostRecharge.stopwatch(boostWait))
			{
				boostRecharge.reset();
				boostActive = true;
			}
			break;
		}
	case DIK_LSHIFT:
		{
			shieldActive = true;
			if(shieldTimer.stopwatch(shieldDecrease) && shieldPercentage > 0)
			{
				shieldPercentage -= 1;
			}
			break;
		}
	}
}

void game_keyRelease(int key)
{
	static Timer delay;

	switch(key) {
	case DIK_GRAVE:
	case DIK_F12:
		console->setShowing(!console->isShowing());
		break;

	case DIK_ESCAPE: 
		if (state == 1) 
		{
			while(!delay.stopwatch(500));
			state = 2;
			g_engine->setPaused(true);
		}
		else if (state == 2) {
			g_engine->Close();
		}
		break;

	case DIK_P: //pause
		if (state == 1)
		{
			g_engine->setPaused(true);
			state = 3;
		}
		else if (state == 3)
		{
			g_engine->setPaused(false);
			state = 1;
		}
		break;			
	case DIK_SPACE: 
		if (state == 0)
		{
			while(!delay.stopwatch(500));
			g_engine->setPaused(false);
			state = 1;
		}

		if (state == 2)
		{
			while(!delay.stopwatch(500));
			resetGame();
			g_engine->setPaused(false);
			state = 0;
		}

		break;
	case DIK_LSHIFT:
		shieldActive = false;
	}
}

void detonateBomb()
{
	double radius = 30.0;

	for(int i = 0; i < boulders.size(); i++)
	{
		Vector3 v(ship->GetPosition().x, ship->GetPosition().y, ship->GetPosition().z);
		v -= boulders[i].getPosition();
		if (v.Length() <= radius)
			boulders[i].setAlive(false);
	}
}

void game_mouseButton(int button) {
	mButton = button;
	switch(button) {
	case 0:
		if(lastShotFired.stopwatch(250))
		{
			fireShot();
			g_engine->audio->Play("gunfire");
		}
		break;
	case 1:
		detonateBomb();
		break;
	}
}
void game_mouseMotion(int x,int y) { }
void game_mouseMove(int x,int y) 
{ 
	xMouse = x;
	yMouse = y;
	Vector3 axis(ship->GetPosition().x,ship->GetPosition().y,ship->GetPosition().z);
	Vector2 mouse(xMouse - g_engine->getScreenWidth() / 2,yMouse - g_engine->getScreenHeight() / 2);
	double angle = acos(mouse.Normal().DotProduct(Vector2(0.0,1.0)));
	if(mouse.getX() < 0)
		angle =  2* PI - angle;
	angle = angle * 180 / PI;
	if(angle >= 0.0 && angle < 180.0)
		angle = 180.0 - angle;
	else
		angle = 540.0 - angle;
	shipRotate = angle;
	
	shipForward->Set(*screenForward);
	shipForward->rotate(axis,angle);
	shipLeft->Set(*screenLeft);
	shipLeft->rotate(axis,angle);
	setShipRotMat();
	//ship->Transform(shipRotMat);
}
void game_mouseWheel(int wheel) { }

void game_end()
{
	if (console) delete console;
	if (camera) delete camera;
	if (earth) delete earth;
	if (ship) delete ship;
	if (cF) delete cF;
	if (cT) delete cT;
	//	if (font10) delete font10;
	if (font24) delete font24;
}


void doExplosion(Sprite *target)
{

}

void game_entityUpdate(Entity *entity)
{
	
}

void game_entityRender(Entity *entity)
{
	
}

void game_entityCollision(Entity *entity1, Entity *entity2)
{
	
}

