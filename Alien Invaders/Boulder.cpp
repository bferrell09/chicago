#include "Boulder.h"

Boulder::Boulder(int mag, Vector3 position, Vector3 velocity, double time, double speed)
{
	magnitude = mag;
	this->velocity = velocity;
	this->time = time;
	isAlive = true;
	this->speed = speed;

	switch(magnitude)
	{
	case 1:
		item = new Mesh();
		item->Load("earth.x");
		item->SetScale(0.125, 0.125, 0.125);
		radius = 2.5;
		mass = 1.0;
	break;
	case 2:
		item = new Mesh();
		item->Load("earth.x");
		item->SetScale(0.25, 0.25, 0.25);
		radius = 5;
		mass = 1.5;
	break;
	case 3:
		item = new Mesh();
		item->Load("earth.x");
		item->SetScale(0.5, 0.5, 0.5);
		radius = 10;
		mass = 2.0;
	break;
	}
	
	setPosition(position);
	orbitalAxis = position.CrossProduct(velocity);
	D3DXMatrixRotationYawPitchRoll(&internRotation, velocity.getX(), velocity.getY(), velocity.getZ());
	D3DXMatrixRotationYawPitchRoll(&rotation, velocity.getX(), velocity.getY(), velocity.getZ());
	acceleration = getPosition();
}

void Boulder::setPosition(double x, double y, double z)
{
	item->SetPosition(x, y, z);
}

void Boulder::setPosition(Vector3 p)
{
	item->SetPosition(p.getX(), p.getY(), p.getZ());
}

Vector3 Boulder::getPosition()
{
	return Vector3(item->GetPosition().x, item->GetPosition().y, item->GetPosition().z);
}

void Boulder::setOrientation(Vector3 l, Vector3 f, Vector3 u)
{
	left = l;
	forward = f;
	up = u;
}

void Boulder::setOrientation(double xl, double yl, double zl, double xf, double yf, double zf, double xu, double yu, double zu)
{
	left.setX(xl);
	left.setY(yl);
	left.setZ(zl);
	forward.setX(xf);
	forward.setY(yf);
	forward.setZ(zf);
	up.setX(xu);
	up.setY(yu);
	up.setZ(zu);
}

void Boulder::update()
{
	//Matrix stuff
	// Calculate new position
	Vector3 pos(getPosition());
	pos.rotate(orbitalAxis,speed);
	setPosition(pos);

	// Set new orientation
	rotation = rotation * internRotation;
	/*Vector3 position = getPosition();
	acceleration = position;
	position = position.getFinalPosition(position, velocity, acceleration, time);
	setPosition(position);
	velocity = velocity.getFinalVelocity(velocity, acceleration, time);*/
}

void Boulder::draw() 
{
	item->draw(rotation);
}
