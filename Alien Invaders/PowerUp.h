#include "..\Engine\Advanced2D.h"

using namespace Advanced2D;

class PowerUp
{
private:
	int id;
	Vector3 orbitalAxis;
	D3DXMATRIX internRotation;
	D3DXMATRIX rotation;
	Vector3 velocity;
	Vector3 acceleration;
	Vector3 left;
	Vector3 forward;
	Vector3 up;
	bool alive;
	double radius;
public:
	Mesh *item;
	PowerUp(int id, Vector3 position, Vector3 velocity);
	int getID() {return id;}
	double getRadius() {return radius;}
	bool getAlive() {return alive;}
	void setAlive(bool life) {this->alive = life;}
	void setAxis(Vector3 a) {orbitalAxis = a;}
	void setAxis(double x, double y, double z) {orbitalAxis.setX(x); orbitalAxis.setY(y); orbitalAxis.setZ(z);}
	Vector3 getAxis() {return orbitalAxis;}
	void Load(char *image) {item->Load(image);}
	void setPosition(double x, double y, double z);
	void setPosition(Vector3 p);
	Vector3 getPosition();
	void update();
	void draw();
};

PowerUp::PowerUp(int id, Vector3 position, Vector3 velocity)
{
	this->id = id;
	this->velocity = velocity;
	alive = true;
	radius = 2.0;
	
	switch(id)
	{
	case 1:
		item = new Mesh();
		item->Load("gunUp.x");
		item->SetScale(0.2, 0.2, 0.2);
		break;
	case 2:
		item = new Mesh();
		item->Load("shieldUp.x");
		item->SetScale(0.2, 0.2, 0.2);
		break;
	case 3:
		item = new Mesh();
		item->Load("shieldUp.x");
		item->SetScale(0.2, 0.2, 0.2);
		break;
	}

	setPosition(position);
	orbitalAxis = position.CrossProduct(velocity);
	D3DXMatrixRotationYawPitchRoll(&internRotation, velocity.getX(), velocity.getY(), velocity.getZ());
	D3DXMatrixRotationYawPitchRoll(&rotation, velocity.getX(), velocity.getY(), velocity.getZ());
	acceleration = getPosition();
}

void PowerUp::setPosition(double x, double y, double z)
{
	item->SetPosition(x, y, z);
}

void PowerUp::setPosition(Vector3 p)
{
	item->SetPosition(p.getX(), p.getY(), p.getZ());
}

Vector3 PowerUp::getPosition()
{
	return Vector3(item->GetPosition().x, item->GetPosition().y, item->GetPosition().z);
}

void PowerUp::update()
{
	//Matrix stuff
	// Calculate new position
	Vector3 pos(getPosition());
	pos.rotate(orbitalAxis,0.5);
	setPosition(pos);

	// Set new orientation
	rotation = rotation * internRotation;
	/*Vector3 position = getPosition();
	acceleration = position;
	position = position.getFinalPosition(position, velocity, acceleration, time);
	setPosition(position);
	velocity = velocity.getFinalVelocity(velocity, acceleration, time);*/
}

void PowerUp::draw() 
{
	item->draw(rotation);
}